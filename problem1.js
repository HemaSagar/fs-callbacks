const fs = require('fs');
const path = require('path');

function problem1() {
    const dirName = "jsonFiles";
    const dirPath = path.join(__dirname, dirName);
    const jsonFiles = new Array(10);

    // creates a directory and create and delete files asynchronously
    makeDirectory();

    function makeDirectory() {

        fs.mkdir(dirPath, (err) => {
            if (err) {
                console.log(`Error while creating ${dirName} directory \n ${err.message}`);
            }
            else {
                console.log(`${dirName} directory created`);

                //jsonfiles creation function called after directory successfully created.
                createJsonFiles();
            }
        })
    }

    //function for creating jsonfiles asynchronously.
    function createFile(fileName, callBack) {
        const filePath = path.join(dirPath, fileName);
        const message = `This is ${fileName}`;

        fs.writeFile(filePath, message, (err) => {
            callBack(err, fileName);

        })
    }

    // Keep track of how many files created and number of errors occured
    let createdFilesLength = 0;
    let errorFilesLength = 0;

    function createJsonFiles() {

        for (let index = 0; index < jsonFiles.length; index++) {
            // creates file names and assign to jsonFiles array
            jsonFiles[index] = `${index + 1}.json`;

            createFile(jsonFiles[index], (err, fileName) => {
                if (err) {
                    console.error(`error occured while creating file ${fileName} ${err.message}` );
                    errorFilesLength += 1;
                }
                else {
                    createdFilesLength += 1;
                    console.log(`${fileName} created`);
                }

                //calls deleteJsonFiles function after creation of all files.
                if ((createdFilesLength + errorFilesLength) === jsonFiles.length) {
                    console.log("files creation done \n");
                    deleteJsonFiles();

                }

            });
        }
    }


    //function to delete the files that are created in the given directory.
    function deleteFiles(fileName, callBack) {
        const currFilePath = path.join(dirPath,fileName);
        fs.unlink(currFilePath, (err) => {
            callBack(err,fileName);
        })

    }

    //keeps track of number of files deleted and error occured file.
    let deletedFilesLength = 0;
    let errorDeletingFiles = 0;

    function deleteJsonFiles() {

        //Read the files of the given directory and then delete all the
        fs.readdir(dirPath, 'utf-8', (err, files) => {
            if (err) {
                console.error("error occured while reading a directory")
            }
            else {
                for (let index = 0; index < files.length; index++) {
                    deleteFiles(files[index], (err,fileName) => {
                        if (err) {
                            console.error(`error accured while deleting ${files[index]}, ${err}`);
                            errorDeletingFiles += 1;
                        }
                        else {
                            console.log(`${files[index]} deleted successfully`);
                            deletedFilesLength += 1;
                        }

                        if ((deletedFilesLength + errorDeletingFiles) === files.length) {
                            console.log("files deletion successful");

                        }

                    })


                }
            }
        });
    }
}


module.exports = problem1;

