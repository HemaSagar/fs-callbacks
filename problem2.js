const { execFileSync } = require('child_process');
const fs = require('fs')
const path = require('path')

function problem2() {

    const fileName = 'lipsum.txt'
    const filePath = path.join(__dirname, fileName);

    const newFileNamesFile = 'newFileNames.txt';
    const newFileNamesFilePath = path.join(__dirname, newFileNamesFile);

    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            console.error(`error occured while reading ${fileName} ${err.message}`);
        }
        else {
            const upperCaseData = data.toUpperCase();
            const upperCaseDataFile = 'rawToUpperCase.txt';
            const upperCaseFilePath = path.join(__dirname, upperCaseDataFile);

            fs.writeFile(upperCaseFilePath, upperCaseData, (err) => {
                if (err) {
                    console.error(`Error occured while writing data to ${upperCaseData} file ${err.message} `)
                }
                else {
                    console.log(`data modified to uppercase written to file ${upperCaseDataFile}`);
                    fs.writeFile(newFileNamesFilePath, upperCaseDataFile, (err) => {
                        if (err) {
                            console.error(`Error occured while writing file name ${upperCaseDataFile} to file ${newFileNamesFile} ${err.message}`)
                        }
                        else {
                            console.log(`${upperCaseDataFile} name added to ${newFileNamesFile} successfully`);

                            fs.readFile(upperCaseFilePath, 'utf-8', (err, data) => {
                                if (err) {
                                    console.error(`Error occured while reading contents of ${upperCaseDataFile} file ${err.message}`);
                                }
                                else {
                                    console.log(`Successfully read data from ${upperCaseDataFile} file`)

                                    const sentences = data.toLowerCase().split('.').map((sentence) => {
                                        return sentence.trim();
                                    })
                                        .filter((sentence) => {
                                            return sentence !== "";
                                        })
                                        .join('\n');

                                    const lowerCaseSentencesFile = 'lowerCaseSentences.txt';
                                    const lowerCaseSentencesFilePath = path.join(__dirname, lowerCaseSentencesFile);

                                    fs.writeFile(lowerCaseSentencesFilePath, sentences, (err) => {
                                        if (err) {
                                            console.error(`Error occured while writing to ${lowerCaseSentencesFile} file ${err.message}`);
                                        }
                                        else {
                                            console.log(`lowercase sentences written successfully to ${lowerCaseSentencesFile} file`);

                                            fs.appendFile(newFileNamesFilePath, `\n${lowerCaseSentencesFile}`, (err) => {
                                                if (err) {
                                                    console.error(`Error occured while writing file name ${lowerCaseSentencesFile} to ${newFileNamesFile}`);
                                                }
                                                else {
                                                    console.log(`${lowerCaseSentencesFile} file name added to ${newFileNamesFile} successfully`);

                                                    fs.readFile(upperCaseFilePath, 'utf-8', (err, data) => {
                                                        if (err) {
                                                            console.error(`Error occured while reading ${upperCaseDataFile} file ${err.message}`);
                                                        }
                                                        else {
                                                            console.log(`Data read successfully from ${upperCaseDataFile} file`);

                                                            let upperCaseSentenceSorted = data.split('.').map((sentence) => {
                                                                return sentence.trim();
                                                            })
                                                            .filter((sentence) => {
                                                                return sentence !== "";
                                                            })
                                                            .sort()
                                                            .join('\n')

                                                            const upperCaseSentenceSortedFile = 'upperCaseSorted.txt'
                                                            const upperCaseSentenceSortedPath = path.join(__dirname, upperCaseSentenceSortedFile);
                                                            fs.writeFile(upperCaseSentenceSortedPath, upperCaseSentenceSorted, (err) => {
                                                                if(err){
                                                                    console.error(`Error occured while writing data to ${upperCaseSentenceSortedFile} file ${err.message}`);
                                                                }
                                                                else{
                                                                    console.log(`uppercase sorted data written successfully to ${upperCaseSentenceSortedFile}`);
                                                                    fs.appendFile(newFileNamesFile, `\n${upperCaseSentenceSortedFile}`, (err)=>{
                                                                        if(err){
                                                                            console.error(`Error occurred while adding file name ${upperCaseSentenceSortedFile} to ${newFileNamesFile} ${err.message}`);
                                                                        }
                                                                        else{
                                                                            console.log(`File added succesfully to ${newFileNamesFile}`);

                                                                            fs.readFile(lowerCaseSentencesFilePath, 'utf-8', (err, data) => {
                                                                                if (err) {
                                                                                    console.error(`Error occured while reading data from ${lowerCaseSentencesFile} file`);
                                                                                }
                                                                                else {
                                                                                    console.log(`${lowerCaseSentencesFile} succesfully read`);
                                                                                    const lowerCaseSortedData = data.split('\n').sort().join('\n');
        
                                                                                    const lowerCaseSortedDataFile = 'loweCaseSortedData.txt';
                                                                                    const lowerCaseSortedFilePath = path.join(__dirname,lowerCaseSortedDataFile);
                                                                                    fs.writeFile(lowerCaseSortedFilePath, lowerCaseSortedData, (err) =>{
                                                                                        if(err){
                                                                                            console.error(`Error occured while writing to ${lowerCaseSortedDataFile} ${err.message}`);
                                                                                        }
                                                                                        else{
                                                                                            console.log(`lowercased sorted data written succesfully to ${lowerCaseSortedDataFile}`);
        
                                                                                            
                                                                                            fs.appendFile(newFileNamesFilePath, `\n${lowerCaseSortedDataFile}`, (err)=>{
                                                                                                if(err){
                                                                                                    console.error(`Error occured while adding ${lowerCaseSortedDataFile} to ${newFileNamesFile}`);
                                                                                                }
                                                                                                else{
                                                                                                    console.log(`${lowerCaseSortedDataFile} added successfully to ${newFileNamesFile}`);
                                                                                                    fs.readFile(newFileNamesFilePath, 'utf-8', (err, data) => {
                                                                                                        if(err){
                                                                                                            console.error(`Error occured while reading the file ${newFileNamesFile} ${err.message}`);
                                                                                                        }
                                                                                                        else{
                                                                                                            console.log(`\n File names successfully read from ${newFileNamesFile}`)
                                                                                                            const fileNames = data.split('\n');
                                                
                                                                                                            let deletedFilesLength = 0;
                                                                                                            let errorFilesLength = 0;
                                                                                                            for (let index = 0; index < fileNames.length ;index++){
                                                
                                                                                                                fs.unlink(fileNames[index], (err) => {
                                                                                                                    if(err){
                                                                                                                        errorFilesLength += 1;
                                                                                                                        console.error(`Error occurred while deleting ${fileNames[index]} file ${err.message}`);
                                                                                                                    }
                                                                                                                    else{
                                                                                                                        deletedFilesLength += 1;
                                                                                                                        console.log(`File ${fileNames[index]} deleted successfully`);
                                                                                                                    }
                                                                                                                    if((deletedFilesLength + errorFilesLength) == fileNames.length){
                                                
                                                                                                                        console.log("all deleted");
                                                                                                                    }
                                                                                                                });
                                                                                                            }
                                                
                                                                                                        }
                                                                                                    })

                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                        }
                                                                    } )
                                                                    
                                                                }
                                                            })
                                                            

                                                                }

                                                            })
                                                        }
                                                    })


                                                    
                                                }
                                            })
                                        }
                                    })

                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = problem2;